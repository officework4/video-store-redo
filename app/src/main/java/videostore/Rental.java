package videostore;

import videostore.movies.Movie;

import java.math.BigDecimal;

class Rental {

    private final Movie movie;
    private final int daysRented;

    public Rental(Movie movie, int daysRented) {
        this.movie = movie;
        this.daysRented = daysRented;
    }

    public int getDaysRented() {
        return daysRented;
    }

    public Movie getMovie() {
        return movie;
    }

    protected BigDecimal calculateRental() {
        return movie.getPrice(daysRented);
    }

    protected int calculatePoints() {
        if ((getMovie().givesBonus()) && getDaysRented() > 1) {
            return 2;
        } else return 1;
    }
}