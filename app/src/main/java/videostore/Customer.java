/**
 *
 */
package videostore;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

class Customer {

    private final String name;

    private final List<Rental> rentals = new ArrayList<>();


    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental rental) {
        rentals.add(rental);
    }

    public String getName() {
        return name;
    }

    private String statementHeader() {
        return ("Rental Record for " + getName() + "\n");
    }

    private String statementBody() {
        String body = "";
        for (Rental rental : rentals) {
            body += String.format("\t%s\t%s\n", rental.getMovie().getTitle(), rental.calculateRental().setScale(1));
        }
        return body;
    }

    private String statementFooter() {
        return String.format("Amount owed is %s\nYou earned %s frequent renter points", calculateTotalAmount(), getFrequentRenterPoints());
    }

    protected String statement() {
        return statementHeader() + statementBody() + statementFooter();
    }


    private BigDecimal calculateTotalAmount() {
        BigDecimal totalAmount = BigDecimal.valueOf(0.0);
        for (Rental rental : rentals) {
            totalAmount = totalAmount.add(rental.calculateRental());
        }
        return totalAmount;
    }

    private int getFrequentRenterPoints() {
        int frequentRenterPoints = 0;
        for (Rental rental : rentals) {
            frequentRenterPoints += rental.calculatePoints();
        }
        return frequentRenterPoints;
    }

}






