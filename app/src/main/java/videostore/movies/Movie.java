package videostore.movies;

import java.math.BigDecimal;

public abstract class Movie {

    abstract public BigDecimal getPrice(int daysRented);

    abstract public String getTitle();

    abstract public boolean givesBonus();



}